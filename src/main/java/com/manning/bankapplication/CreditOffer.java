package com.manning.bankapplication;

import java.util.ArrayList;
import java.util.List;

public abstract class CreditOffer {
    private String id;
    List<Customer>  customerList = new ArrayList<>();

    public CreditOffer(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public abstract boolean addCustomer(Customer customer);
    public abstract boolean removeCustomer(Customer customer);
}
