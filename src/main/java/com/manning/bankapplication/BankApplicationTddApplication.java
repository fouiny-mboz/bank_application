package com.manning.bankapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankApplicationTddApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankApplicationTddApplication.class, args);
    }

}
