package com.manning.bankapplication;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class BankTest1 {

    @DisplayName("Given there is an economy credit offer")
    @Nested
    class EconomyCreditOfferTest{
        private CreditOffer economyCreditOffer;
        private BusinessCreditOffer businessCreditOffer;
        private Customer mike;
        private Customer john;

        @BeforeEach
        void setup(){
            economyCreditOffer = new EconomyCreditOffer("1");
            mike = new Customer("Mike", false);
            john = new Customer("John",true);
        }

        @Nested
        @DisplayName("When we have a usual customer")
        class UsualCustomer{
            @Test
            @DisplayName("Then you can add and remove him from an economy credit offer")
            public void testEconomyCreditOfferUsualCustomer(){
                assertAll("Verify all condition for usual customer" ,
                        ()  ->assertEquals("1", economyCreditOffer.getId()),
                        ()  ->assertEquals(true, economyCreditOffer.addCustomer(mike)),
                        () ->assertEquals(1, economyCreditOffer.getCustomerList().size()),
                        ()  ->assertEquals("Mike", economyCreditOffer.getCustomerList().get(0).getName()),
                        () -> assertTrue(economyCreditOffer.getCustomerList().contains(mike)),
                        ()  -> assertEquals(true, economyCreditOffer.removeCustomer(mike)),
                        ()  -> assertEquals(0, economyCreditOffer.getCustomerList().size())
                        );
            }
        }

        @Nested
        @DisplayName("When we have a VIP customer")
        class VipCustomer {
            @Test
            @DisplayName("Then you can add him but you cannot remove him")
            public void testEconomyCreditOfferVipCustomer(){
                assertAll("Verify all conditions for a VIP customer and economy",
                        () -> assertEquals("1", economyCreditOffer.getId()),
                        () -> assertEquals(true, economyCreditOffer.addCustomer(john)),
                        () -> assertEquals(1,economyCreditOffer.getCustomerList().size()),
                        ()  -> assertTrue(economyCreditOffer.getCustomerList().contains(john))
                );
            }

            @BeforeEach
            void setUtp(){
                businessCreditOffer = new BusinessCreditOffer("2");
                mike = new Customer("Mike", false);
                john = new Customer("John", true);
            }

            @Nested
            @DisplayName("When we have a usual customer")
            class UsualCustomer{
                @Test
                @DisplayName("Then you cannot add or remove him a business credit offer")
                public void testBusinessCreditOfferUsualCustomer(){
                    assertAll("Verify all conditions for usual customer and business offer",
                            () -> assertEquals(false,businessCreditOffer.addCustomer(mike)),
                            () -> assertEquals(0, businessCreditOffer.getCustomerList().size()),
                            ()-> assertEquals(false, businessCreditOffer.removeCustomer(mike))
                    );
                }
            }

            @Nested
            @DisplayName("When you have a VIP customer")
            class VipCustomerBusiness {
                @Test
                @DisplayName("Then you can add him but cannot remove a business customer")
                public void testBusinessCreditOfferVipCustomer(){
                    assertAll("Verify all conditions for a VIP customer business offer",
                            ()  -> assertEquals(true, businessCreditOffer.addCustomer(john)),
                            () -> assertEquals(1, businessCreditOffer.getCustomerList().size()),
                            ()  -> assertEquals(false, businessCreditOffer.removeCustomer(john))
                    );
                }
            }
        }


    }
}
