package com.manning.bankapplication;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BankTest {

    @Test
    public void testEconomyCreditOfferUsualCustomer(){
        CreditOffer economyCreditOffer = new EconomyCreditOffer("1");
        Customer mike = new Customer("john", false);

        assertEquals("1", economyCreditOffer.getId());
        assertEquals(true, economyCreditOffer.addCustomer(mike));
        assertEquals(1, economyCreditOffer.customerList.size());
        assertEquals("john", economyCreditOffer.getCustomerList().get(0).getName());
        assertEquals(false, economyCreditOffer.getCustomerList().get(0).isVip());

        assertEquals(true,economyCreditOffer.removeCustomer(mike));
        assertEquals(0,economyCreditOffer.getCustomerList().size());
    }

    @Test
    public void testBusinessCreditOfferUsualCustomer(){
        CreditOffer businessCreditOffer = new BusinessCreditOffer("2");

        Customer mike = new Customer("mike", false);

        assertEquals(false, businessCreditOffer.addCustomer(mike));
        assertEquals(false, businessCreditOffer.removeCustomer(mike));
        assertEquals(0, businessCreditOffer.getCustomerList().size());
    }

    @Test
    public void testEconomyCreditOfficeVipCustomer(){
        CreditOffer economyCreditOffer = new EconomyCreditOffer("1");
        Customer tony = new Customer("Tony", true);

        assertEquals(true, economyCreditOffer.addCustomer(tony));
        assertEquals(false, economyCreditOffer.removeCustomer(tony));
        assertEquals(1, economyCreditOffer.getCustomerList().size());
        assertEquals("Tony", economyCreditOffer.getCustomerList().get(0).getName());
        assertEquals(true, economyCreditOffer.getCustomerList().get(0).isVip());
    }

    @Test
    public void testBusinessCreditOfferVipCustomer(){
        CreditOffer businessCreditOffer = new BusinessCreditOffer("2");
        Customer tony = new Customer("Tony", true);
        Customer thoma = new Customer("Thoma", true);

        assertEquals(true, businessCreditOffer.addCustomer(tony));
        assertEquals(true, businessCreditOffer.addCustomer(thoma));
        assertEquals(2, businessCreditOffer.getCustomerList().size());
        assertEquals(false, businessCreditOffer.removeCustomer(tony));
        assertEquals("Thoma", businessCreditOffer.getCustomerList().get(1).getName());
    }

}
